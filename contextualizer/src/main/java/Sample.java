import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Sample code for reading category and URL data.
 */
public class Sample {

    public static void main(String[] args) throws IOException {
        ///////////////////////////////////////////////////////////////////////////
        // Properties in form of String (key) -> String (value)...               //
        // Note that the the value represents a comma separated list of keywords //
        ///////////////////////////////////////////////////////////////////////////
        Properties properties = loadProperties();
        List<String> lines = getLines("http://en.wikipedia.org/wiki/Toy");
    }

    private static Properties loadProperties() throws IOException {
        Properties properties = new Properties();
        properties.load(Sample.class.getResourceAsStream("categories.properties"));
        return properties;
    }

    private static List<String> getLines(String path) throws IOException {
        URL url = new URL(path);
        LineNumberReader reader = new LineNumberReader(new InputStreamReader(url.openConnection().getInputStream()));
        List<String> lines = new ArrayList<String>();
        String line = "";
        while (line != null) {
            line = reader.readLine();
            lines.add(line);
        }
        return lines;
    }

}
