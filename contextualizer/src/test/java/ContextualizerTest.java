import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ContextualizerTest {

    @Test
    public void testWikiLinks() {
        Contextualizer contextualizer = new Contextualizer();
        String financeCategory = contextualizer.contextualize("http://en.wikipedia.org/wiki/Finance");
        assertEquals("finance", financeCategory);
        String toysCategory = contextualizer.contextualize("http://en.wikipedia.org/wiki/Toy");
        assertEquals("toys", toysCategory);
    }

}
